<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GalleryRequest;
use App\Gallery;
use App\Video;
use Illuminate\Database\QueryException as Exception;
use Session;
class GalleriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $galleries = Gallery::paginate(15);
        return view('galleries.index', compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $videos = Video::select('id', 'provider', 'title', 'source', 'display')->get();
        $selected_videos = [];

        return view('galleries.create', compact('videos', 'selected_videos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  GalleryRequest $request
     *
     * @return void
     */
    public function store(GalleryRequest $request)
    {
        try {
            $data = $request->only(['name', 'description', 'display']);
            $gallery = Gallery::create($data);

            $video_ids = $request->input('video_ids');
            $gallery->videos()->attach($video_ids);

            Session::flash('message', 'Gallery added!');
            return redirect('galleries');
        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage())
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Gallery $gallery
     *
     * @return void
     */
    public function show(Gallery $gallery)
    {
        $videos = $gallery->videos()->get();

        return view('galleries.show', compact('gallery', 'videos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Gallery $gallery
     *
     * @return void
     */
    public function edit(Gallery $gallery)
    {
        $videos = Video::select('id', 'provider', 'title', 'source', 'display')->get();
        $selected_videos = $gallery->videos()->pluck('id')->toArray();

        return view('galleries.edit', compact('gallery', 'videos', 'selected_videos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Gallery $gallery
     *
     * @param  GalleryRequest $request
     *
     * @return void
     */
    public function update(Gallery $gallery, GalleryRequest $request)
    {
        try {
            $data = $request->only(['name', 'description', 'display']);
            $gallery->update($data);

            $video_ids = $request->input('video_ids');
            $gallery->videos()->sync($video_ids);

            Session::flash('message', 'Gallery updated!');
            return redirect('galleries');
        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage())
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Gallery $gallery
     *
     * @return void
     */
    public function destroy(Gallery $gallery)
    {
        try {
            $name = $gallery->name;
            $gallery->delete();
            Session::flash('message', $name . ' has been moved to the Trash.');
            return redirect('galleries');
        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }


}
