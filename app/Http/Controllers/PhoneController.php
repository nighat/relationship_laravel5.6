<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Phone;
use App\User;
class PhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $phones = Phone::latest()->paginate(5);
        return view('phone.index',compact('phones','user'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

         $users = User::all();
        return view('phone.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'user_id' => 'required|exists:users,id',
            'phone' => 'required',
        ]);

        $user = User::find($request->user_id);
      //  $phone = new phone;
        //$phone->user_id = $request->user_id;
       // $phone->phone = $request->phone;
       // $phone->save();
       //$user->phone()->save($phone);


      $user->phone()->create(['phone' =>$request->phone]);


       // Phone::create($request->all());
//        Phone::create([
//
//            'user_id'=>$request->user_id,
//            'phone'=>$request->phone,
//
//        ]);


        return redirect(route('phone.index'))->withMessage('Phone Added');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $phone = Phone::find($id);
        return view('phones.show')->with('phones',$phone);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phone = Phone::where('id',$id)->first();
        return view('phones.edit',compact('phone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $phone = phone::find($id);
        $phone->phone = $request->phone;
        $phone->save();

        return redirect(route('phones.index'))->withMessage('phones Is Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Phone::where('id',$id)->delete();
        return redirect()->back()->withMessage('phone Is Deleted');
    }
}
