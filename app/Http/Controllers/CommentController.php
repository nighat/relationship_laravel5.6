<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $comments = Comment::latest()->paginate(5);
        return view('comments.index', compact('comments'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $posts = Post::all();
        return view('comments.create', compact('posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'post_id' => 'required|exists:posts,id',
            'comment' => 'required',
        ]);


        $post = Post::find($request->post_id);
        $post->comments()->create([
            'comment'=> $request->comment

        ]);
//    <___________________________________>
//      //  dd($post->comments());
////        $comment = new Comment;
////
////        $comment->comment = $request->comment;
////        $comment->post_id = $request->post_id;
////        $comment->save();
//       // $comment->post()->save();
//         <___________________________________>
//



      return redirect(route('comments.index'))->withMessage('Comment Added');

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::find($id);
        return view('comments.show')->with('comment', $comment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::where('id', $id)->first();
        return view('comments.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $comment = Comment::find($id);
        $comment->comment = $request->comment;
        $comment->post_id = $request->post_id;
        $comment->save();

        return redirect(route('comments.index'))->withMessage('comments Is Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comment::where('id', $id)->delete();
        return redirect()->back()->withMessage('comment Is Deleted');
    }
}