<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{


    protected $fillable = [
        'name', 'description', 'display',
    ];
    protected $dates = ['deleted_at'];
   public function videos(){

       return $this->belongsToMany('App\Video','gallery_video');
   }
}
