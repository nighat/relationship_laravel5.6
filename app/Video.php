<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Video extends Model
{
    use SoftDeletes;

    protected $fillable = ['provider', 'title', 'summary', 'source', 'display'];

//    protected $unguarded = [
//        'id', 'created_at', 'updated_at', 'deleted_at',
//    ];
    protected $dates = ['deleted_at'];

    public function galleries(){

        return $this->belongsToMany('App\Gallery');
    }
}
