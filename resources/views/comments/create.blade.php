@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                    <div class="panel-heading"><h3 style="text-align: center">Comment Create Form</h3>
                    </div>

                    <div class="panel-body">

                        <div class=" col-lg-offset-3 col-lg-6">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        <form class="form-horizontal" method="POST" action="{{ route('comments.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}




                            <div class="form-group">
                                <label for="comment" class="col-md-4 control-label">comment</label>

                                <div class="col-md-6">
                                    <input id="comment" type="text" class="form-control" name="comment" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Select Id</label>
                                <select class="form-control select2 select2-hidden-accessible" data-placeholder="Select a State" style="width: 100%;" tabindex="-1" aria-hidden="true" name="post_id">
                                    @foreach($posts as $post)
                                        <option value="{{ $post->id }}">{{ $post->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">

                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('comments.index') }}" class=" btn btn-danger">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>

            </div>
        </div>
    </div>
@endsection