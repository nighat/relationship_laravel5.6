@extends('layouts.app')

{{-- Page title --}}

@section('content')


    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    comment Information
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                            <tr>
                                <th> {{ trans('Comment') }} </th>
                                <td> {{ $comment->comment }} </td>
                            </tr>
                            <tr>
                                <th> {{ trans('Post Id') }} </th>
                                <td> {{ $comment->post_id }} </td>
                            </tr>

                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="2" class="text-right">
                                    <a href="{{ url('comments/' . $comment->id . '/edit') }}"
                                       class="btn btn-primary btn-xs" title="Edit User"><span
                                                class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['comments', $comment->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete User',
                                            'onclick'=>'return confirm("Are you sure you want to delete ' . $comment->name . '?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>


                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection