@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel-heading"><h5 style="text-align: center">Slider Edit Form</h5></div>


                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('comments.update',$comment->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label for="post_id" class="col-md-4 control-label">Post Id</label>

                            <div class="col-md-6">
                                <input id="post_id" type="number" class="form-control" name="post_id" value="{{ $comment->post_id }}">
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="comment" class="col-md-4 control-label">Comment</label>

                            <div class="col-md-6">
                                <input id="comment" type="text" class="form-control" name="comment" value="{{ $comment->comment }}" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">

                                <button type="submit" class="btn btn-primary">Submit</button>

                                <a href="{{ route('comments.create') }}" class=" btn btn-danger">Create</a>
                                <a href="{{ route('comments.index') }}" class=" btn btn-danger">Comment</a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection