@extends('layouts.app')

{{-- Page title --}}
{{--@section('title', 'Add Gallery')--}}

@section('content')

    {{--<div class="row">--}}
        {{--<div class="col-lg-12">--}}
            {{--<h1 class="page-header" style="text-align: center;color: #259d6d">Add Gallery</h1>--}}
        {{--</div>--}}
        {{--<!-- /.col-lg-12 -->--}}
    {{--</div>--}}

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    User Add Form
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/users', 'class' => 'form-horizontal']) !!}

                    <div class="form-group {{ $errors->has('name') ? 'bg-success' : ''}}">
                        {!! Form::label('name', trans('User.Name'), ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('name', '<p class="bg-danger">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                        {!! Form::label('email', trans('User.Email'), ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::text('email', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                        {!! Form::label('password', trans('User.password'), ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-9">
                            {!! Form::password('password', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection