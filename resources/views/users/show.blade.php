@extends('layouts.app')

{{-- Page title --}}
@section('title')
    {{ $user->name }}
    @parent
@stop

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ $user->name }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    User Information
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                            <tr>
                                <th> {{ trans('User.name') }} </th>
                                <td> {{ $user->name }} </td>
                            </tr>
                            <tr>
                                <th> {{ trans('User.email') }} </th>
                                <td> {{ $user->email }} </td>
                            </tr>
                            <tr>
                                <th> {{ trans('User.password') }} </th>
                                <td> {{ $user->password }} </td>
                            </tr>

                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="2" class="text-right">
                                    <a href="{{ url('users/' . $user->id . '/edit') }}"
                                       class="btn btn-primary btn-xs" title="Edit User"><span
                                                class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['users', $user->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete User',
                                            'onclick'=>'return confirm("Are you sure you want to delete ' . $user->name . '?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>


                </div>

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection