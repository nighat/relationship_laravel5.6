@extends('layouts.app')

{{-- Page title --}}
@section('title', 'Edit' . $user->name )

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit {{ $user->name }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    User Edit Form
                </div>
                <div class="panel-body">
                    {!! Form::model($user, [
                        'method' => 'PATCH',
                        'url' => ['/users', $user->id],
                        'class' => 'form-horizontal'
                    ]) !!}

                    @include('users.form')

                    {!! Form::close() !!}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection