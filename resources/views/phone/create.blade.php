@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Phone Add Form


                </div>

    <form role="form" action="{{ route('phone.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="box-body">

            <div class=" col-lg-offset-3 col-lg-6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="form-group">
                    <label>Select Id</label>
                    <select class="form-control select2 select2-hidden-accessible" data-placeholder="Select a State" style="width: 100%;" tabindex="-1" aria-hidden="true" name="user_id">
                        @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="phone">Phone Number</label>
                    <input type="number" class="form-control" id="phone"  name="phone" placeholder="phone">
                </div>




                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a  href="{{ route('phone.index') }}" class="btn btn-warning">Back</a>
                </div>
            </div>

        </div>


     </form>
        <!-- /.col-lg-12 -->
    </div>



@endsection