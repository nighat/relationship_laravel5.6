@extends('layouts.app')

{{-- Page title --}}
@section('title', 'Many To Many')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Phone Number
                <a href="{{ url('/phone/create') }}" class="btn btn-primary btn-xs" title="Add New Gallery"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Phone List
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        @include('layouts.alert')
                        <table class="table table-striped table-bordered table-hover" id="dataTables">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th> {{ trans('User Name') }} </th>
                                <th> {{ trans('Phone.Number') }} </th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $x=1; @endphp
                            @foreach($phones as $item)
                                <tr>
                                    <td>{{ $x++ }}</td>
                                    <td>{{ $item->user->name}}</td>
                                    <td>{{ $item->phone }}</td>

                                    <td>
                                        <a href="{{ url('/phone/' . $item->id) }}" class="btn btn-success btn-xs" title="View Gallery"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                        <a href="{{ url('/phone/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs"
                                           title="Edit phone"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/phone', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Gallery" />', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete phone',
                                            'onclick'=>'return confirm("Are you sure you want to delete ' . $item->name . '?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination"> {!! $phones->render() !!} </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection