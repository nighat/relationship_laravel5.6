
    @extends('layouts.app')

    {{-- Page title --}}

@section('content')

    <div class="row">
        {{--<div class="col-lg-12">--}}
            {{--<h1 class="page-header">Post</h1>--}}
        {{--</div>--}}
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                {{--<div class="panel-heading">--}}
                  {{--Post Information--}}
                {{--</div>--}}
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <tbody>
                            <tr>
                                <td style="text-align: center;color:darkblue"> <h3>{{ $post->name }}</h3> </td>
                            </tr>

                            </tbody>

                        </table>
                        <div class="row">
                        <div class="col-md-8 col-md-offset-4">

                                    @foreach($post->comments as $comment)
                                        <tr>
                                            <td ><b>Comment</b><br/></td>
                                        </tr>
                                         <tr>
                                            <td>{{ $comment->comment }}<br/></td>
                                        </tr>
                                    @endforeach
                                </div>
                        </div>

                        <form class="form-horizontal" method="POST" action="{{ route('comments.store') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="post_id" class="form-control" value="{{ $post->id}}">

                            <div class="form-group">
                                <label for="comment" class="col-md-4 control-label">Comment</label>

                                <div class="col-md-4">
                                    <input id="comment" type="text" class="form-control" name="comment" >
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                   <button type="submit" class="btn btn-primary"> Submit</button>
                                   {{--<button type="submit" class="btn btn-primary">Submit</button>--}}

                                </div>
                            </div>
                        </form>
                    </div>


                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection