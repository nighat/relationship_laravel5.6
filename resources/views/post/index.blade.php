@extends('layouts.app')

@section('content')
    <div class="panel-heading">
        <h5 style="text-align: center">post Lists <a href="{{ route('post.create') }}" class=" btn btn-danger pull-right">Add New post</a></h5>
        <div style="background:#00b38f; color: #ffffff; width: 600px;text-align: center; font-size: 20px;">{{ session('message') }}</div>


    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Name</th>

            <th>Action</th>

        </tr>
        </thead>
        <tbody>

        @foreach($posts as $post)

            <tr>
                <td>{{ $loop->index + 1}}</td>

                {{--<td><a href=" {{ $post->link }}"><img style="height: 200px; width: 200px" src="{{storage_path('app/public'.$post->image)}}"></a></td>--}}

                <td>{{ $post->name }}</td>

                <td> <a href="{{ route('post.show',$post->id) }}" class=" btn btn-danger"><span class=" glyphicon glyphicon-eye-open"></span></a>
                    <a href="{{ route('post.edit',$post->id) }}" class=" btn btn-info"><span class="glyphicon glyphicon-edit"></span></a>
                    <form id="delete-form-{{ $post->id }}" method="POST" action="{{ route('post.destroy',$post->id) }}" style="display: none">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                    </form>
                    <a href="" onclick="
                            if(confirm('Are you sure, You went to delete this?'))

                            {
                            event.preventDefault();
                            document.getElementById('delete-form-{{ $post->id }}').submit();
                            }
                            else{
                            event.preventDefault();
                            }
                            " class=" btn btn-info"><span class="glyphicon glyphicon-trash"></span></a>

                </td>


            </tr>
        @endforeach

        </tbody>

    </table>

    {{ $posts->links() }}

@endsection