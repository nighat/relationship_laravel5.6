@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel-heading"><h3 style="text-align: center">Post Create Form</h3>
                    <div style="background:#00b38f; color: #ffffff; width: 600px;text-align: center; font-size: 20px;">{{ session('message') }}</div>

                </div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('post.update',$post->id)}}">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name"  value="{{ $post->name }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">

                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{ route('post.index') }}" class=" btn btn-danger">Back</a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection