<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            {{--<a class="navbar-brand" href="{{ url('/') }}">--}}
            {{--{{ config('app.name', 'Laravel') }}--}}
            {{--</a>--}}
            {{--<a href="{{ route('home') }}">--}}
            {{--<img src="{{ asset('images/icc.png') }}" alt="CCCL">--}}
            {{--</a>--}}
            <ul class="nav navbar-nav navbar-reft">
                <li><a href="{{ route('users.index') }}" class="bg-{{ (request()->segment(1) == 'Galleries' ? 'danger' : 'info') }}"><b>User</b></a></li>
                <li><a href="{{ route('phone.index') }}" class="bg-{{ (request()->segment(1) == 'videos' ? 'danger' : 'info') }}"><b>Phone</b></a></li>
                <li><a href="{{ route('galleries.index') }}" class="bg-{{ (request()->segment(1) == 'Galleries' ? 'danger' : 'info') }}"><b>Gallery</b></a></li>
                <li><a href="{{ route('videos.index') }}" class="bg-{{ (request()->segment(1) == 'videos' ? 'danger' : 'info') }}"><b>Video</b></a></li>
                <li><a href="{{ route('post.index') }}" class="bg-{{ (request()->segment(1) == 'Galleries' ? 'danger' : 'info') }}"><b>Post</b></a></li>
                <li><a href="{{ route('comments.index') }}" class="bg-{{ (request()->segment(1) == 'videos' ? 'danger' : 'info') }}"><b>Comment</b></a></li>

            </ul>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            {{--<ul class="nav navbar-nav navbar-right">--}}
                {{--<!-- Authentication Links -->--}}
                {{--@guest--}}
                {{--<li><a href="{{ route('login') }}"><b>Login</b></a></li>--}}
                {{--<li><a href="{{ route('register') }}"><b>Register</b></a></li>--}}
                {{--@else--}}
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>--}}
                            {{--{{ Auth::users()->name }} <span class="caret"></span>--}}
                        {{--</a>--}}

                        {{--<ul class="dropdown-menu">--}}
                            {{--<li>--}}
                                {{--<a href="{{ route('logout') }}"--}}
                                   {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
                                    {{--Logout--}}
                                {{--</a>--}}

                                {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                    {{--{{ csrf_field() }}--}}
                                {{--</form>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--@endguest--}}
            {{--</ul>--}}
        </div>
    </div>
</nav>